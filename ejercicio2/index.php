<?php
// autocarga de clases

use clases\Imagen;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$imagen1 = new Imagen("1.jpg", true, 100);
$imagen2 = new Imagen("2.jpg", false, 150);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <?= $imagen1 ?>
    <?= $imagen2 ?>
</body>

</html>