<?php

namespace clases;

class Imagen
{
    private string $src;
    private bool $border = false;
    private ?int $ancho = null;
    private ?int $alto = null;

    const RUTA = "../images/";
    const PLANTILLA = __NAMESPACE__ . "/plantilla.blade.php";

    public function __construct(string $src, bool $border = false, ?int $ancho = null, ?int $alto = null)
    {
        $this->src = $src;
        $this->border = $border;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    /*
        tostring realizado directamente con un string
    */
    // public function __toString()
    // {
    //     return "<img src='" . self::RUTA . "$this->src' border='$this->border' width='$this->ancho' height='$this->alto'>";
    // }

    /**
     * toString realizado con un control de flujo
     */
    // public function __toString()
    // {
    //     ob_start();
    //     // asigno a las variables de la plantilla
    //     // los valores a utilizar
    //     $src = self::RUTA . "$this->src";
    //     $border = $this->border;
    //     $ancho = $this->ancho;
    //     $alto = $this->alto;
    //     // cargo la plantilla
    //     include "plantilla.php";
    //     // devuelvo el contenido del buffer
    //     return ob_get_clean();
    // }

    /**
     * voy a utilizar una plantilla
     */
    public function __toString()
    {
        $salida = file_get_contents(self::PLANTILLA);
        return str_replace(
            [
                "{{ src }}",
                "{{ border }}",
                "{{ ancho }}",
                "{{ alto }}"
            ],
            [
                self::RUTA . "$this->src",
                $this->border ?: "0",
                $this->ancho,
                $this->alto
            ],
            $salida
        );
    }
}
