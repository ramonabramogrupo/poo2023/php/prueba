<?php


// autocarga de clases

use clases\Consultas;

spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

if (!isset($_GET['id'])) {
    header('Location: indexConsultas.php');
}

// creo una conexion
$conexion = new mysqli('localhost', 'root', '', 'pruebaPhp');

// creo una consulta
$consulta = new Consultas($conexion, "autores");

// inserto el registro
$consulta->eliminar($_GET['id']);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="row mt-3">
        <?php
        require "_menu.php";
        ?>
    </div>
    <div class="container mt-3">
        <div class="alert alert-success">
            Registro eliminado
        </div>
        <a class="btn btn-primary" href="indexConsultas.php">Volver</a>
    </div>

</body>

</html>