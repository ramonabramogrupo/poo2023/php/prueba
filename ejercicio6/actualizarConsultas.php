<?php

use clases\Consultas;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$mensaje = "";

// llegan los datos del formulario
if ($_POST) {
    // creo una conexion
    $conexion = new mysqli('localhost', 'root', '', 'pruebaPhp');

    // creo una consulta
    $consulta = new Consultas($conexion, "autores");

    // leo los datos del formulario
    $datos = [
        "nombre" => $_POST["nombre"],
        "apellidos" => $_POST["apellidos"]
    ];

    // preparo un nuevo registro
    $consulta->setDatos($datos);

    // inserto el registro
    $consulta->actualizar($_GET["id"]);

    $mensaje = "Registro actualizado";
} elseif (isset($_GET["id"])) {
    // compruebo si vengo del listado general para modificar un registro

    // creo una conexion
    $conexion = new mysqli('localhost', 'root', '', 'pruebaPhp');

    // creo una consulta
    $consulta = new Consultas($conexion, "autores");

    // saco los datos del registro a modificar
    $datos = $consulta->findOne($_GET["id"]);
} else {
    header("Location: indexConsultas.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="row mt-3">
        <?php
        require "_menu.php";
        ?>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Insertar Registro con Clase Consultas</h1>
                <div class="lead">
                    Introducir registro en la tabla autores
                </div>
            </div>
        </div>
        <?php
        // mostrar mensaje de registro insertado
        if (!empty($mensaje)) {
        ?>
            <div class="row mt-3">
                <div class="alert alert-success">
                    <?= $mensaje ?>
                </div>
            </div>
        <?php
        }
        ?>
        <div class="row mt-3">
            <form method="post" class="col-lg-6">
                <div class="input-group mb-3">
                    <span class="input-group-text">Nombre</span>
                    <input type="text" name="nombre" class="form-control" value="<?= $datos["nombre"] ?>">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text">Apellidos</span>
                    <input type="text" name="apellidos" class="form-control" value="<?= $datos["apellidos"] ?>">
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">
                        Actualizar
                    </button>
                    <button type="reset" class="btn btn-danger">
                        Borrar
                    </button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>