<?php

namespace clases; // esto es para que las clases se puedan acceder desde la rama de ejercicios de rama

use mysqli;
use mysqli_result;

abstract class Modelo
{
    // conexion con el servidor de base de datos
    private mysqli $db;

    // campo que es clave principal
    private string $pk;

    // array con los nombres de los campos
    protected ?array $campos;

    // devuelve el nombre de la tabla
    abstract static protected function tabla(): string;


    public function __construct()
    {
        $this->db = new mysqli(
            "localhost",
            "root",
            "",
            "pruebaPhp"
        );

        // leo la clave principal de la tabla
        $this->pk = self::obtenerPk();
    }

    public function obtener(array $datos): self
    {
        // recorro el array asociativo de datos y lo asigno a las propiedades del objeto
        foreach ($this->campos as $campo) {
            $this->$campo = $datos[$campo];
        }
        return $this;
    }

    public function insert()
    {
        // voy a colocar los valores en un texto
        $valores = "(";
        foreach ($this->campos as $campo) {
            $valores .= "'" . $this->$campo . "',";
        }

        // añado el parentesis de cierre y quito la ultima coma
        $valores[strlen($valores) - 1] = ")";

        // creo un string con el nombre de los campos 
        $campos = implode(",", $this->campos);


        $consulta = "INSERT INTO " . static::tabla() . " ($campos) VALUES {$valores};";
        $this->db->query($consulta);
        return $this;
    }

    public function actualizar(int $id)
    {

        $actualizar = "SET ";
        $buscar = "WHERE ";
        $principal = $this->pk;
        foreach ($this->campos as $campo) {
            $actualizar .= "$campo='" . $this->$campo . "',";
        }

        $buscar .= "$principal='" . $id . "'";

        // quito la ultima coma
        $actualizar[strlen($actualizar) - 1] = " ";

        // creo la consulta de actualizar
        $consulta = "UPDATE " . static::tabla() . " {$actualizar} {$buscar}";
        $this->db->query($consulta);
    }



    static public function eliminar(int $id)
    {
        $db = new mysqli("localhost", "root", "", "pruebaPhp");
        $consulta = "DELETE FROM " . static::tabla() . " WHERE id = $id";
        $db->query($consulta);
        $db->close();
    }

    static public function listar(): array
    {
        $consulta = "SELECT * FROM " . static::tabla();
        $db = new mysqli("localhost", "root", "", "pruebaPhp");
        $resultados = $db->query($consulta);
        $db->close();
        return $resultados->fetch_all(MYSQLI_ASSOC);
    }

    static public function findAll(): mysqli_result
    {
        $consulta = "SELECT * FROM " . static::tabla();
        $db = new mysqli("localhost", "root", "", "pruebaPhp");
        $resultados = $db->query($consulta);
        $db->close();
        return $resultados;
    }

    static public function findOne(int $id): object
    {
        $consulta = "SELECT * FROM " . static::tabla() . " WHERE " . self::obtenerPk() . "= $id";
        $db = new mysqli("localhost", "root", "", "pruebaPhp");
        $resultados = $db->query($consulta);
        $db->close();
        return $resultados->fetch_object();
    }

    static public function gridView(array $botones = [], array $campos = []): string
    {
        $registros = self::listar();
        if (count($registros) > 0) {
            // mostrar los registros
            $salida = "<table class='table table-striped table-bordered'>";
            $salida .= "<thead class='table-dark'><tr>";

            // compruebo si el usuario me ha pasado los campos a mostrar
            if (count($campos) == 0) {
                // si no me ha pasado los campos los leo del primer registro de la base de datos
                $campos = array_keys($registros[0]);
            }

            foreach ($campos as $campo) {
                $salida .= "<th>$campo</th>";
            }
            if (count($botones) > 0) {
                // añado una columna para los botones
                $salida .= "<td>Acciones</td>";
            }

            $salida .= "</tr></thead>";
            // muestro todos los registros
            foreach ($registros as $registro) {
                $salida .= "<tr>";
                // mostrando los campos que hayamos seleccionado
                foreach ($campos as $campo) {
                    $salida .= "<td>" . $registro[$campo] . "</td>";
                }
                // mostrando los botones
                if (count($botones) > 0) {
                    $salida .= "<td>";

                    foreach ($botones as $label => $enlace) {
                        $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a> ";
                    }

                    // cerramos la celda
                    $salida .= "</td>";
                }

                $salida .= "</tr>";
            }
            $salida .= "</table>";
        } else {
            $salida = "No hay registros";
        }
        return $salida;
    }
    static private function obtenerPk(): string
    {
        $consulta = "SHOW KEYS FROM " . static::tabla() . " WHERE Key_name = 'PRIMARY'";
        $db = new mysqli("localhost", "root", "", "pruebaPhp");
        $resultados = $db->query($consulta);
        $db->close();
        return $resultados->fetch_assoc()['Column_name'];
    }
}
