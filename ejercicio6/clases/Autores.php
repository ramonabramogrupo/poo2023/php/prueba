<?php

namespace clases;

use mysqli;
use mysqli_result;

class Autores extends Modelo
{
    // campos de la tabla
    public int $id;
    public string $nombre;
    public string $apellidos;

    // campos para la asignacion masiva
    protected ?array $campos = ["nombre", "apellidos"];

    static protected function tabla(): string
    {
        return "autores";
    }
}
