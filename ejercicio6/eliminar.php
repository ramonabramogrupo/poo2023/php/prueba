<?php

use clases\Autores;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

if (!isset($_GET['id'])) {
    header('Location: index.php');
}

// elimino el registro
Autores::eliminar($_GET['id']);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eliminar</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="row mt-3">
        <?php
        require "_menu.php";
        ?>
    </div>
    <div class="container mt-3">
        <div class="alert alert-success">
            Registro eliminado
        </div>
        <a class="btn btn-primary" href="index.php">Volver</a>
    </div>
</body>

</html>