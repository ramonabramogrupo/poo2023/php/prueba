<?php


// autocarga de clases

use clases\Consultas;

spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

// creo una conexion
$conexion = new mysqli('localhost', 'root', '', 'pruebaPhp');

// creo una consulta
$consulta = new Consultas($conexion, "autores");

// pruebo a listar
$salida = $consulta->findAll()->listar([
    "eliminar" => "eliminarConsultas.php",
    "actualizar" => "actualizarConsultas.php"
]);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="row mt-3">
        <?php
        require "_menu.php";
        ?>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Listar con Consultas</h1>
                <div class="lead">
                    Listado de registros de la tabla autores
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <?= $salida ?>
        </div>
    </div>
</body>

</html>