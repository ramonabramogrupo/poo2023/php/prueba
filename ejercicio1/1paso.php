<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paso 1</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="container">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Ejercicio Numero 1 del Examen de PHP</h1>
                <div class="lead">
                    Escriba un numero entre 0 y 10. Se crear una tabla de ese numero de filas.
                </div>
            </div>
        </div>

        <form class="row mt-3 p-3 bg-light" action="2paso.php" method="post">
            <div class="mb-3">
                <h2 class="text-color1">
                    Paso 1
                </h2>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text">Tamaño de la tabla</span>
                <input type="number" name="numero" class="form-control" required>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">
                    Paso 2
                </button>
                <button type="reset" class="btn btn-danger">
                    Borrar
                </button>
            </div>
        </form>

        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                Ejercicio del examen de PHP - Ramon Abramo
            </div>
        </div>

        <div class="row mt-3">
            <img src="../imgs/logo.png" class="d-block mx-auto col-lg-2 col-sm-3">
        </div>
</body>

</html>