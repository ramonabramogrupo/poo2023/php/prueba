<?php
// en caso de intentar acceder al paso 2 sin pasar por el paso 1
// le redirecciono al paso 1
if (!$_POST) {
    header("Location: 1paso.php");
}
// leemos el numero de cajas a dibujar
$numeroCajas = $_POST["numero"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paso 2</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="container">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Ejercicio Numero 1 del Examen de PHP</h1>
                <div class="lead">
                    Escribe algo en las cajas de texto
                </div>
            </div>
        </div>

        <form class="row mt-3 p-3 bg-light" action="3paso.php" method="post">
            <div class="mb-3">
                <h2 class="text-color1">
                    Paso 2
                </h2>
            </div>
            <?php
            for ($contador = 0; $contador < $numeroCajas; $contador++) {
                include "_cajas.php";
            }
            ?>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">
                    Paso 3
                </button>
                <button type="reset" class="btn btn-danger">
                    Borrar
                </button>
            </div>
        </form>
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                Ejercicio del examen de PHP - Ramon Abramo
            </div>
        </div>

        <div class="row mt-3">
            <img src="../imgs/logo.png" class="d-block mx-auto col-lg-2 col-sm-3">
        </div>
</body>

</html>