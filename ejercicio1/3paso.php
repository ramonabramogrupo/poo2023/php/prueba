<?php

/**
 * funciones 
 */
// tengo que comprobar si todas las cajas contienen algo
// tengo que indicar cuantas estan vacias y cuantas hay en total
function vacias(array $vector): int
{
    $contadorVacias = 0;
    foreach ($vector as $valores) {
        //if ($valores == "") {
        if (empty($valores)) {
            $contadorVacias++;
        }
    }
    return $contadorVacias;
}

function vacias1(array $vector): int
{
    // inicializo el contador
    // suponiendo que no hay cajas vacias
    $contadorVacias = 0;

    // creo un array de repeticiones
    $arrayRepeticiones = array_count_values($vector);

    // busco dentro del array de repeticiones el indice vacio
    if (array_key_exists("", $arrayRepeticiones)) {
        $contadorVacias = $arrayRepeticiones[""];
    }
    return $contadorVacias;
}

// tengo que comprobar si hay repetidos en los datos recibidos
function repetidos(array $vector): bool
{
    $arrayRepeticiones = array_count_values($vector);
    $salida = true; // supongo que hay repeticiones

    // compruebo si hay repeticiones
    // si el array original es igual que el array de repeticiones
    // es que no hay repeticiones
    if (count($arrayRepeticiones) == count($vector)) {
        $salida = false;
    }

    return $salida;
}

function repetidos1(array $vector): bool
{
    $salida = true;

    // el mismo array pero sin repetidos
    $sinRepetidos = array_unique($vector);

    // si el array original es igual que el array sin repetidos
    // es que no hay repeticiones
    if (count($sinRepetidos) == count($vector)) {
        $salida = false;
    }

    return $salida;
}

/**
 * control de acceso
 */

// si intentas ir al paso3 directamente te mando al paso 1
if (!$_POST) {
    header('Location: 1paso.php');
}


/**
 * procesamiento
 */
$valores = $_POST["caja"];

$numeroVacias = vacias1($valores);
$total = count($valores);
$rellenas = $total - $numeroVacias;
$hayRepeticiones = repetidos1($valores);

/**
 * salida
 */

// crear los mensajes de salida
if (!$numeroVacias) { // $numeroVacias == 0
    $mensaje = "Has rellenado las {$total} cajas";
} else {
    $mensaje = "Hay {$rellenas} cajas rellenas de un total de {$total}";
}

$mensaje1 = $hayRepeticiones ? "Hay repeticiones" : "No hay repeticiones";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paso 3</title>
    <link rel="stylesheet" href="../css/home.css">
</head>

<body>
    <div class="container">
        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                <h1>Ejercicio Numero 1 del Examen de PHP</h1>
                <div class="lead">
                    Paso 3 - Estadisticas
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="card col-lg-4 color1 text-light">
                <div class="card-header ">
                    <h3>Resultados</h3>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $mensaje ?></p>
                    <p class="card-text"><?= $mensaje1 ?></p>
                </div>
            </div>

            <div class="col-lg-4 card offset-lg-3 p-2">
                <div class="card-body">
                    <h3 class="card-title">Valores escritos</h3>
                </div>
                <div class="card-text">
                    <ul class="list-group">
                        <?php
                        foreach ($valores as $valor) {
                            echo "<li class='list-group-item'>";
                            echo $valor ?: "Vacía";
                            echo "</li>";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="color1 rounded p-3 text-white">
                Ejercicio del examen de PHP - Ramon Abramo
            </div>
        </div>
        <div class="row mt-3">
            <img src="../imgs/logo.png" class="d-block mx-auto col-lg-2 col-sm-3">
        </div>
</body>

</html>