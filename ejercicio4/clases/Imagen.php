<?php

namespace clases;

class Imagen
{
    private string $src;
    private bool $border = false;
    private ?int $ancho = null;
    private ?int $alto = null;

    const RUTA = "../images/";
    const PLANTILLA = __NAMESPACE__ . "/plantilla.blade.php";

    public function __construct(string $src, bool $border = false, ?int $ancho = null, ?int $alto = null)
    {
        $this->setSrc($src);
        $this->setBorder($border);
        $this->setAncho($ancho);
        $this->setAlto($alto);
    }

    /*
        tostring realizado directamente con un string
    // */
    // public function __toString()
    // {
    //     $salida = "<img";
    //     $salida .= $this->getSrc() . $this->getBorder() . $this->getAncho() . $this->getAlto();
    //     $salida .= ">";
    //     return $salida;
    // }

    /**
     * toString realizado con un control de flujo
     */
    // public function __toString()
    // {
    //     ob_start();
    //     // asigno a las variables de la plantilla
    //     // los valores a utilizar
    //     $src = $this->getSrc();
    //     $border = $this->getBorder();
    //     $ancho = $this->getAncho();
    //     $alto = $this->getAlto();
    //     // cargo la plantilla
    //     include "plantilla.php";
    //     // devuelvo el contenido del buffer
    //     return ob_get_clean();
    // }

    /**
     * voy a utilizar una plantilla
     */
    public function __toString()
    {
        $salida = file_get_contents(self::PLANTILLA);
        return str_replace(
            [
                "{{ src }}",
                "{{ border }}",
                "{{ ancho }}",
                "{{ alto }}"
            ],
            [
                $this->getSrc(),
                $this->getBorder(),
                $this->getAncho(),
                $this->getAlto()
            ],
            $salida
        );
    }

    /**
     * Get the value of src
     *
     * @return string
     */
    public function getSrc(): string
    {
        $salida = " src='" . self::RUTA . "{$this->src}' ";
        return $salida;
    }

    /**
     * Set the value of src
     *
     * @param string $src
     *
     * @return self
     */
    public function setSrc(string $src): self
    {

        // comprobar que la imagen existe
        if (file_exists(self::RUTA . $src)) {
            $this->src = $src;
        } else {
            exit("No existe imagen");
        }
        return $this; // añado esto para que sea fluent
    }

    /**
     * Get the value of border
     *
     * @return bool
     */
    public function getBorder(): string
    {
        return " border='" . (int)$this->border . "' ";
    }

    /**
     * Set the value of border
     *
     * @param bool $border
     *
     * @return self
     */
    public function setBorder(bool $border): self
    {

        $this->border = $border;

        return $this;
    }

    /**
     * Get the value of ancho
     *
     * @return ?int
     */
    public function getAncho(): string
    {
        if (empty($this->ancho)) {
            return "";
        } else {
            return " width='" . $this->ancho . "' ";
        }
    }

    /**
     * Set the value of ancho
     *
     * @param ?int $ancho
     *
     * @return self
     */
    public function setAncho(?int $ancho): self
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get the value of alto
     *
     * @return ?int
     */
    public function getAlto(): string
    {
        if (empty($this->alto)) {
            return "";
        } else {
            return " height='" . $this->alto . "' ";
        }
    }

    /**
     * Set the value of alto
     *
     * @param ?int $alto
     *
     * @return self
     */
    public function setAlto(?int $alto): self
    {
        $this->alto = $alto;

        return $this;
    }
}
