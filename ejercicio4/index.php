<?php
// autocarga de clases

use clases\Imagen;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$imagen1 = new Imagen("1.jpg", true, 100);
$imagen2 = new Imagen("2.jpg", false);
$imagen3 = new Imagen("2.jpg", true, 100, 200);



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <?= $imagen1 ?>
    <?= $imagen2 ?>
    <?php

    $imagen2->setSrc("1.jpg");
    echo $imagen2;
    echo $imagen3;
    ?>
</body>

</html>